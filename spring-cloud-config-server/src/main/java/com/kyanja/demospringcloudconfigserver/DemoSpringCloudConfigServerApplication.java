package com.kyanja.demospringcloudconfigserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@SpringBootApplication
@EnableConfigServer
public class DemoSpringCloudConfigServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoSpringCloudConfigServerApplication.class, args);
    }

}
