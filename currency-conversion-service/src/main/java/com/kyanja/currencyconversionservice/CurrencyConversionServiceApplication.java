package com.kyanja.currencyconversionservice;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients("com.kyanja.currencyconversionservice")
@EnableDiscoveryClient
public class CurrencyConversionServiceApplication implements CommandLineRunner {

    @Value("${spring.profiles.active}")
    private String environmentSetting;


    public static void main(String[] args) {
        SpringApplication.run(CurrencyConversionServiceApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        System.out.println("Currently active profile - " + environmentSetting);
    }
}
