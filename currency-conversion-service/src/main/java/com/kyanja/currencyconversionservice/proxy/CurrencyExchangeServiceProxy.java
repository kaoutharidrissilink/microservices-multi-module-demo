package com.kyanja.currencyconversionservice.proxy;


import com.kyanja.currencyconversionservice.dto.CurrencyConversionBean;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

//Enabling feign
//@FeignClient(name="currency-exchange-service", url="localhost:8082")
@FeignClient(name = "currency-exchange-service")
//enabling ribbon
@RibbonClient(name = "currency-exchange-service")
public interface CurrencyExchangeServiceProxy {

    @GetMapping("/api/v1/currency-exchange-service-api/currency-exchange/from/{from}/to/{to}")
    CurrencyConversionBean getCurrencyConversion(@PathVariable("from") String from, @PathVariable("to") String to);
}
